import '@testing-library/jest-dom';
import Enzyme, { shallow, render, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';


require('jest-fetch-mock').enableMocks();

Enzyme.configure({ adapter: new Adapter() });

export { shallow, mount, render };
export default Enzyme;
