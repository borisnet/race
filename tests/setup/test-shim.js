global.requestAnimationFrame = (callback) => {
    return setTimeout(callback, 0);
};
