module.exports = mode => {
    mode.cache(false);
    const plugins = [
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-transform-classes',
        '@babel/plugin-transform-flow-strip-types',
        'babel-plugin-macros',
        '@babel/plugin-transform-spread',
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-transform-template-literals',
        '@babel/plugin-transform-regenerator',
        '@babel/plugin-transform-shorthand-properties',
        '@babel/plugin-transform-parameters',
        '@babel/plugin-transform-destructuring',
        '@babel/plugin-transform-computed-properties',
        '@babel/plugin-transform-arrow-functions',
        '@babel/plugin-transform-for-of',
    ].filter(Boolean);

    const presets = [
        ['@babel/preset-env', {
            modules: 'auto',
            targets: {
                ie: '11',
                node: 'current'
            }
        }, "jest"],
        ['@babel/preset-react', {
            development: true,
            targets: {
                ie: '11',
            } }],
    ];

    const env = {
       test: {
          plugins: ["@babel/plugin-transform-runtime"]
        }
    };

    return { plugins, presets, env };
};
