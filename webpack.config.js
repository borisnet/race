const HtmlWebpackPlugin = require('html-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');

module.exports = (env, args) => {
  const { mode } = args;

  return {
    entry: './src/index.js',
    target: process.env.MODE === 'development' ? 'web' : 'browserslist',
    output: {
      path: `${__dirname}/dist`,
      publicPath: '/',
      filename: '[name].[contenthash].js',
      clean: true,
      chunkFilename: '[id].js',
    },
    resolve: {
      extensions: ['', '.js', '.jsx', '.ts', '.tsx'],
    },
    optimization: {
      moduleIds: 'deterministic',
      runtimeChunk: 'single',
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendors',
            chunks: 'all',
          },
        },
      },
    },
    devServer: {
      contentBase: ['./src', './public'],
      inline: true,
      historyApiFallback: true,
      hot: true,
    },
    module: {
      rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      }, {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['source-map-loader'],
        enforce: 'pre',
      }, {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      }, {
        test: /\.(s[ac]ss|css)$/i,
          use: [
            // Creates `style` nodes from JS strings
            'style-loader',
            // Translates CSS into CommonJS
            'css-loader',
            'sass-loader',
          ],
        },
        {
          test: /\.(jpe?g|png|gif|svg)$/i,
          use: ['file-loader'],
        },
      ],
    },
    plugins: [
      new WebpackManifestPlugin({
        basePath: '/dist',
      }),
        new HtmlWebpackPlugin({
          template: 'public/index.html',
          filename: './index.html',
          favicon: 'public/favicon.ico',
          manifest: 'dist/manifest.json',
          title: 'Races',
          hash: true,
        }),
      ],
      devtool: (mode === 'development') ? 'source-map' : false,
    mode,
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000,
    },
  };
  };
