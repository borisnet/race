import React from 'react';
import Races from './features/races';

function App() {
  return (
    <div className="App">
        <Races />        
    </div>
  );
}

export default App;
