import camelize from 'camelize';
import moment from 'moment';

export const humanDateDiff = (duration) => {
    const hrs = ~~(duration / 3600);
    const mins = ~~((duration % 3600) / 60);
    const secs = ~~duration % 60;

    let ret = '';

    if (hrs > 0) {
        ret += `${hrs}:${mins < 10 ? '0' : ''}`;
    }

    ret += `${mins}:${secs < 10 ? '0' : ''}`;
    ret += `${secs}`;
    return ret;
};

export const selector = (rawData) => {
    const summaries = Object.values(camelize(rawData));
    const sorted = summaries.sort((a, b) => {
        if (a.advertisedStart.seconds > b.advertisedStart.seconds) {
            return 1;
        }
        if (b.advertisedStart.seconds > a.advertisedStart.seconds) {
            return -1;
        }

        return 0;
    });

    const minuteAgo = moment().subtract(1, 'minutes');
    return sorted.filter(race => moment.unix(race.advertisedStart.seconds).isAfter(minuteAgo));
};
