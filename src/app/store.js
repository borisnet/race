import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';
import { racesApi } from '../features/races/slice';

export const store = configureStore({
  reducer: {
    [racesApi.reducerPath]: racesApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(racesApi.middleware),
});

setupListeners(store.dispatch);
