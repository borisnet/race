import React, { useState, useEffect } from 'react';

import { Error, Loading } from '../atoms';
import { useGetRacesQuery } from './slice';
import Filters from './Filters';
import RaceRow from './RaceRow';

const Races = () => {
    const { data: allRaces, error, isLoading } = useGetRacesQuery(50, {
        pollingInterval: 1000,
    });

    const [cat, setCat] = useState('');
    const [races, setRaces] = useState(allRaces);

    useEffect(() => {
        if (cat) {
            const filtered = allRaces.filter(race => race.categoryId === cat);
            return setRaces(filtered.slice(0, 5));
        }
        if (allRaces) {
            setRaces(allRaces.slice(0, 5));
        }
    }, [allRaces, cat]);

    if (error) {
        return (<Error />);
    }

    return (
      <div>
        <Filters cat={cat} setCat={setCat} />
        <div className="container mt-5">
          <div className="row mt-5">
            <div className="col-12">
              {isLoading && (<Loading />)}
              {!isLoading && races && (
                <table className="table table-dark">
                  <thead>
                    <tr>
                      <th>Number</th>
                      <th>Name</th>
                      <th>Starts In</th>
                    </tr>
                  </thead>
                  <tbody className="text-start">
                    {races.map((race) => <RaceRow key={race.raceId} race={race} />)}
                  </tbody>
                </table>
                )}
            </div>
          </div>
        </div>
      </div>
    );
};

export default Races;
