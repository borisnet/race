import React from 'react';
import { ApiProvider } from '@reduxjs/toolkit/query/react';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { racesApi } from './slice';

  describe('Races API Slice', () => {
    test('Races ApiProvider allows a user to make queries using Redux RTK toolkit', async () => {
      function RacesMock() {
        const [value, setValue] = React.useState(0);

        const { isFetching } = racesApi.endpoints.getRaces.useQuery(1, {
          skip: value < 1,
        });

        return (
          <div>
            <div data-testid="isFetching">{String(isFetching)}</div>
            <div onClick={() => setValue((val) => val + 1)}>
              Increment value
            </div>
          </div>
        );
      }

      const { getByText, getByTestId } = render(
        <ApiProvider api={racesApi}>
          <RacesMock />
        </ApiProvider>
      );

      await waitFor(() =>
        expect(getByTestId('isFetching').textContent).toBe('false')
      );
      fireEvent.click(getByText('Increment value'));
      await waitFor(() =>
        expect(getByTestId('isFetching').textContent).toBe('true')
      );
      await waitFor(() =>
        expect(getByTestId('isFetching').textContent).toBe('false')
      );
      fireEvent.click(getByText('Increment value'));
      // Being that nothing has changed in the args, this should never fire.
      expect(getByTestId('isFetching').textContent).toBe('false');
    });
  });
