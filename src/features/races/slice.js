import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { selector } from '../../app/helpers';

export const racesApi = createApi({
  reducerPath: 'races',
  baseQuery: fetchBaseQuery({
      baseUrl: 'https://vast-bayou-98041.herokuapp.com/https://api.neds.com.au/rest/v1/racing/',
    }),
  endpoints: (builder) => ({
    getRaces: builder.query({
      query: (count) => `?method=nextraces&count=${count}`,
      transformResponse: (response) => {
          if (response && response.data) {
              return selector(response.data.race_summaries);
          }
          return response;
      },
    }),
  }),
});

export const { useGetRacesQuery } = racesApi;
