import React from 'react';
import moment from 'moment';

import { humanDateDiff } from '../../app/helpers';

export default ({ race }) => {
    const getTimer = (advertisedStart) => {
        const diff = (-moment().diff(moment.unix(advertisedStart.seconds), 'seconds'));
        if (diff < 1) {
            return (
              <span>Started!!!</span>
            );
        }
        return (
          <span>{humanDateDiff(diff)}</span>
        );
    };

    return (
      <tr key={race.raceId}>
        <th scope="row">{race.raceNumber}</th>
        <td>{race.raceName}</td>
        <td>{getTimer(race.advertisedStart)}</td>
      </tr>
      );
};
