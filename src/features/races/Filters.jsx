import React from 'react';

const Filters = ({ cat, setCat }) => {
    const cats = {
        '9daef0d7-bf3c-4f50-921d-8e818c60fe61': 'Greyhound',
        '161d9be2-e909-4326-8c2c-35ed71fb460b': 'Harness',
        '4a2788f8-e825-4d36-9894-efd4baf1cfae': 'Horse',
    };

    const handleCat = (e) => {
        setCat(e.target.value);
    };

    return (
      <nav className="navbar navbar-dark bg-dark">
        <div className="container-fluid">
          <div className="navbar-brand clickable">Races</div>
          <div className="d-flex ml-auto navbar-nav justify-content-end">
            <select className="form-select form-select-lg" value={cat} onChange={handleCat}>
              <option key="any" value="">All Categories</option>
              {Object.keys(cats).map((key) => {
                            return (
                              <option key={key} value={key}>{cats[key]}</option>
                            );
                        })}
            </select>
          </div>
        </div>
      </nav>
    );
};

export default Filters;
