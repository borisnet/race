import React from 'react';

export default ({ error }) => {
    return (
      <div className="alert alert-danger">
        <p>
          {error}
        </p>
      </div>
    );
};
