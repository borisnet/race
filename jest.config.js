module.exports = {
    testEnvironment: 'jest-environment-jsdom',
    verbose: true,
    setupFilesAfterEnv: [
        "<rootDir>/tests/setup/test-setup.js"
    ]
}